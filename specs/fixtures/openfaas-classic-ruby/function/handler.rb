# frozen_string_literal: true
require 'json'

class Handler
  UUID_FILE = 'uuid.txt'

  def run(req)
    JSON.dump(
      if sanity_file_exists?
        {
          message: 'ok',
          event: sanity_event?(req),
          sanity: sanity_file_contents
        }
      else
        { message: 'Sanity UUID file is missing!' }
      end
    )
  end

  private

  def sanity_file_path
    File.join(__dir__, UUID_FILE)
  end

  def sanity_event?(req)
    JSON.parse(req)['payload'] == 'json'
  rescue
    false
  end

  def sanity_file_exists?
    File.exist?(sanity_file_path)
  end

  def sanity_file_contents
    raise ArgumentError unless sanity_file_exists?

    File.read(sanity_file_path)
  end
end