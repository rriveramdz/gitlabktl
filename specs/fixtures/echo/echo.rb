class Echo
  UUID_FILE = 'uuid.txt'.freeze

  def initialize(event)
    @event = event
  end

  def sanity_file_path
    File.join(__dir__, UUID_FILE)
  end

  def sanity_file_exists?
    File.exists?(sanity_file_path)
  end

  def sanity_file_contents
    raise ArgumentError unless sanity_file_exists?

    File.read(sanity_file_path)
  end

  def sanity_event
    @event.to_h['payload'] == 'json'
  end

  def sanity_env_contents
    ENV['SANITY']
  end

  def self.to_hash(event:, context:)
    Echo.new(event).yield_self do |test|
      if test.sanity_file_exists?
        { message: 'ok',
          event: test.sanity_event,
          sanity: test.sanity_file_contents }
      else
        { message: 'Sanity UUID file is missing!' }
      end
    end
  end
end
