RSpec.shared_examples 'serverless functions deployment' do |function_name|
  describe 'deployed services' do
    let(:service) do
      knative_services.find do |service|
        service.name == "gitlabktl-tests-#{function_name}"
      end
    end

    it 'deploys a one serverless function' do
      functions = knative_services.select do |service|
        service.name.start_with?("gitlabktl-tests-#{function_name}")
      end

      expect(functions).to be_one
    end

    it 'assigns a sanity environment variable' do
      expect(service.env)
        .to include(name: 'SANITY', value: 'sanity-test')
    end
  end

  describe 'deployed functions' do
    let(:function) do
      Serverless::Function.find("gitlabktl-tests-#{function_name}")
    end

    it 'has a URL assigned' do
      expect(function).to have_url
    end

    it 'responds with a valid payload' do
      function.post!(payload: 'json') do |response|
        expect(response.code).to eq 200
        expect(response['message']).to eq 'ok'
        expect(response['event']).to eq true
        expect(response['sanity'].strip).to eq function.sha
      end
    end
  end
end