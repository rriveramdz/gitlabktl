require 'spec_helper'
require_relative 'functions_shared_examples'

RSpec.describe 'Function using the OpenFaaS classic Ruby runtime' do
  include_examples 'serverless functions deployment', 'openfaas-classic-ruby'
end
