module Kubernetes
  module Helpers
    def knative_cluster
      @knative_cluster ||= Kubernetes::Knative::Cluster.fabricate!
    end

    def knative_services
      @knative_services ||= knative_cluster.services
    end
  end
end
