module Kubernetes
  module Knative
    class Service
      def initialize(resource)
        raise ArgumentError unless resource.kind == 'Service'

        @service = resource
      end

      def name
        @service.dig(:metadata, :name)
      end

      def namespace
        @service.dig(:metadata, :namespace)
      end

      def url
        @service.dig(:status, :url) || legacy_url
      end

      def env
        (@service.dig('spec', 'template', 'spec', 'containers', 0, 'env') || legacy_env)
          .to_a
          .map { |resource| { name: resource.name, value: resource.value } }
      end

      private

      # For compatibility with Knative < 0.7
      def legacy_env
        @service.dig(*%w[spec runLatest configuration revisionTemplate spec container env])
      end

      # For compatibility with Knative < 0.6
      def legacy_url
        "http://#{@service.dig(:status, :domain)}"
      end
    end
  end
end
