package registry

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
)

func TestImageString(t *testing.T) {
	image := Image{
		Repository: "registry.example/my/repository",
	}
	assert.Equal(t, "registry.example/my/repository:latest", image.ToString())

	image = Image{
		Repository: "registry.example/my/repository",
		Tag:        "latest",
	}
	assert.Equal(t, "registry.example/my/repository:latest", image.ToString())

	image = Image{
		Repository: "registry.example/my/repository",
		Tag:        "testing",
	}
	assert.Equal(t, "registry.example/my/repository:testing", image.ToString())
}

func TestImageLocations(t *testing.T) {
	image := Image{
		Repository: "my.registry/project",
		Tag:        "0.3.0",
		Aliases:    []string{"latest", "test"},
	}

	locations := image.Locations()

	assert.Equal(t, "my.registry/project:0.3.0", locations[0])
	assert.Equal(t, "my.registry/project:latest", locations[1])
	assert.Equal(t, "my.registry/project:test", locations[2])
}

func TestImageLocationsWithMissingTag(t *testing.T) {
	image := Image{
		Repository: "my.registry/project",
		Aliases:    []string{"latest", "test"},
	}

	assert.Panics(t, func() { image.Locations() })
}

func TestDefaultImageRepository(t *testing.T) {
	image := Image{Tag: "1.0"}

	env.WithStubbedEnv(env.Stubs{"CI_REGISTRY_IMAGE": "my.registry/abc"}, func() {
		assert.Equal(t, "my.registry/abc:1.0", image.ToString())
	})
}

func TestCustomPullRegistry(t *testing.T) {
	image := Image{
		Repository: "registry.example/my/repository",
		Tag:        "latest",
		Username:   "my-user",
	}

	registry := image.PullRegistry()

	assert.Equal(t, "registry.example", registry.Host)
	assert.Equal(t, "my-user", registry.Username)
	assert.Empty(t, registry.Password)
}

func TestCustomPushRegistry(t *testing.T) {
	image := Image{
		Repository: "registry.example/my/repository",
		Tag:        "latest",
		Username:   "my-user",
	}

	registry := image.PushRegistry()

	assert.Equal(t, "registry.example", registry.Host)
	assert.Equal(t, "my-user", registry.Username)
	assert.Empty(t, registry.Password)
}

func TestCustomRegistry(t *testing.T) {
	image := Image{
		Repository: "registry.example/my/repository",
		Tag:        "latest",
		Username:   "my-user",
	}

	registry := image.customRegistry()

	assert.Equal(t, "registry.example", registry.Host)
	assert.Equal(t, "my-user", registry.Username)
	assert.Empty(t, registry.Password)
}

func TestDefaultImagePushRegistry(t *testing.T) {
	image := Image{
		Repository: "my.registry/my/repository",
		Tag:        "latest",
	}
	environment := env.Stubs{
		"CI_REGISTRY":          "my.registry",
		"CI_REGISTRY_IMAGE":    "my.registry/project",
		"CI_REGISTRY_USER":     "my-username",
		"CI_REGISTRY_PASSWORD": "my-password",
	}

	env.WithStubbedEnv(environment, func() {
		registry := image.PushRegistry()

		assert.Equal(t, "my.registry", registry.Host)
		assert.Equal(t, "my-username", registry.Username)
		assert.Equal(t, "my-password", registry.Password)
	})
}

func TestDefaultPullRegistry(t *testing.T) {
	image := Image{
		Repository: "my.registry/my/repository",
		Tag:        "latest",
	}

	environment := env.Stubs{
		"CI_REGISTRY":        "my.registry",
		"CI_REGISTRY_IMAGE":  "my.registry/project",
		"CI_DEPLOY_USER":     "my-username",
		"CI_DEPLOY_PASSWORD": "my-password",
	}

	env.WithStubbedEnv(environment, func() {
		registry := image.PullRegistry()

		assert.Equal(t, "my.registry", registry.Host)
		assert.Equal(t, "my-username", registry.Username)
		assert.Equal(t, "my-password", registry.Password)
	})
}

func TestCustomCredentialsPushRegistry(t *testing.T) {
	image := Image{
		Repository: "my.registry/my/repository",
		Tag:        "latest",
		Username:   "abc",
		Password:   "dbe",
	}

	environment := env.Stubs{
		"CI_REGISTRY":          "my.registry",
		"CI_REGISTRY_IMAGE":    "my.registry/project",
		"CI_REGISTRY_USER":     "my-username",
		"CI_REGISTRY_PASSWORD": "my-password",
	}

	env.WithStubbedEnv(environment, func() {
		registry := image.PushRegistry()

		assert.Equal(t, "my.registry", registry.Host)
		assert.Equal(t, "abc", registry.Username)
		assert.Equal(t, "dbe", registry.Password)
	})
}

func TestCustomCredentialsPullRegistry(t *testing.T) {
	image := Image{
		Repository: "my.registry/my/repository",
		Tag:        "latest",
		Username:   "abc",
		Password:   "dbe",
	}
	environment := env.Stubs{
		"CI_REGISTRY":        "my.registry",
		"CI_REGISTRY_IMAGE":  "my.registry/project",
		"CI_DEPLOY_USER":     "my-username",
		"CI_DEPLOY_PASSWORD": "my-password",
	}

	env.WithStubbedEnv(environment, func() {
		registry := image.PullRegistry()

		assert.Equal(t, "my.registry", registry.Host)
		assert.Equal(t, "abc", registry.Username)
		assert.Equal(t, "dbe", registry.Password)
	})
}
