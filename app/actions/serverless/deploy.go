package serverless

import (
	"fmt"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/k8s"
	"gitlab.com/gitlab-org/gitlabktl/knative"
	"gitlab.com/gitlab-org/gitlabktl/registry"
	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

// DeployAction struct is going to be populated with CLI invocation arguments.
type DeployAction struct {
	DryRun         bool   `short:"t" long:"dry-run" env:"GITLAB_DRYRUN" description:"Dry run only"`
	ServerlessFile string `short:"f" long:"file" env:"GITLAB_SERVERLESS_FILE" description:"Path to serverless manifest file"`
	KubeConfigFile string `short:"c" long:"kubeconfig" env:"GITLAB_KUBECONFIG_FILE" description:"Path to kubeconfig"`
}

func (action *DeployAction) Execute(context *commands.Context) (err error) {
	manifest, err := action.Manifest()
	if err != nil {
		return fmt.Errorf("could not parse serverless manifest: %w", err)
	}

	deployer, err := knative.NewFunctionsDeployer(manifest, action)
	if err != nil {
		return fmt.Errorf("could not create functions deployer: %w", err)
	}

	if action.DryRun {
		_, err = deployer.DeployDryRun()
	} else {
		_, err = deployer.Deploy(context.Ctx)
	}

	return err
}

func (action *DeployAction) Registry() knative.Registry {
	source := registry.NewWithPullAccess()

	return knative.Registry{
		Host:       source.Host,
		Username:   source.Username,
		Password:   source.Password,
		Repository: registry.DefaultRepository(),
	}
}

func (action *DeployAction) Namespace() string {
	return env.Getenv("KUBE_NAMESPACE")
}

func (action *DeployAction) Manifest() (serverless.Manifest, error) {
	if len(action.ServerlessFile) > 0 {
		return serverless.NewServerlessConfigYAML(action.ServerlessFile)
	}

	return serverless.NewServerlessConfig()
}

func (action *DeployAction) KubeConfig() string {
	return k8s.NewKubeConfigPath(action.KubeConfigFile)
}

func newDeployCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "serverless/deploy",
			Config: commands.Config{
				Name:        "deploy",
				Aliases:     []string{},
				Usage:       "Deploy your serverless services",
				Description: "This commands deploys your images to Knative cluster",
			},
		},
		Handler: new(DeployAction),
	}
}

func RegisterDeployCommand(register *commands.Register) {
	register.RegisterCommand(newDeployCommand())
}
