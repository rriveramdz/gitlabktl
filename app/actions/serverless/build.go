package serverless

import (
	"fmt"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/builder"
	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

// BuildAction struct is going to be populated with CLI invocation arguments.
type BuildAction struct {
	DryRun bool `short:"t" long:"dry-run" env:"GITLAB_DRYRUN" description:"Dry run only"`
}

func (action *BuildAction) Execute(context *commands.Context) (err error) {
	manifest, err := serverless.NewServerlessConfig()
	if err != nil {
		return fmt.Errorf("invalid serverless manifest: %w", err)
	}

	for _, function := range manifest.ToFunctions() {
		executor, err := builder.NewFromRuntime(function.ToRuntime())
		if err != nil {
			return err
		}

		if action.DryRun {
			_, err = executor.BuildDryRun()
		} else {
			err = executor.Build(context.Ctx)
		}

		if err != nil {
			return err
		}
	}

	return nil
}

func newBuildCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "serverless/build",
			Config: commands.Config{
				Name:        "build",
				Aliases:     []string{},
				Usage:       "Build your serverless services",
				Description: "This commands reads serverless.yml and builds your images",
			},
		},
		Handler: new(BuildAction),
	}
}

func RegisterBuildCommand(register *commands.Register) {
	register.RegisterCommand(newBuildCommand())
}
