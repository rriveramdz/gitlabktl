package function

import (
	"gitlab.com/gitlab-org/gitlabktl/app/commands"
)

func newFunctionCategory() commands.Category {
	return commands.Category{
		Registrable: commands.Registrable{
			Path: "function",
			Config: commands.Config{
				Name:        "function",
				Usage:       "Manage your serverless functions",
				Aliases:     []string{"fn", "func"},
				Description: "Manage your serverless functions",
			},
		},
	}
}

func RegisterCategory(register *commands.Register) {
	register.RegisterCategory(newFunctionCategory())
}
