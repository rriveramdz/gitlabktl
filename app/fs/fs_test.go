package fs

import (
	"testing"

	"github.com/spf13/afero"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMkdirAllPath(t *testing.T) {
	WithTestFs(func() {
		exists, err := afero.DirExists(afs, "/tmp/my")
		require.NoError(t, err)
		require.False(t, exists)

		err = MkdirAllPath("/tmp/my/dir/file.rb")
		require.NoError(t, err)

		exists, err = afero.DirExists(afs, "/tmp/my/dir")
		require.NoError(t, err)
		assert.True(t, exists)
	})
}
