package commands

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
)

func TestStubbedEnvContext(t *testing.T) {
	env.WithStubbedEnv(env.Stubs{"MY_ENV": "my env"}, func() {
		context := newContext(new(cli.Context))

		assert.Equal(t, "my env", context.Getenv("MY_ENV"))
	})
}
