package runtime

import (
	"context"
	"errors"
	"fmt"
	"path"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/logger"
)

const classicRuntimeRepo = "https://github.com/openfaas/templates"
const ignoreDirectory = "function"
const templateDirectory = "template"

type OpenfaasRuntime struct {
	Details
}

func (runtime OpenfaasRuntime) BuildDryRun() (string, error) {
	_, runtimeName := path.Split(runtime.FunctionRuntime)

	if len(runtimeName) == 0 {
		message := fmt.Sprintf("runtime name \"%s\" is incomplete", runtime.FunctionRuntime)
		err := errors.New(message)
		logger.WithError(err).Warn(message)

		return message, err
	}

	message := fmt.Sprintf("using OpenFaaS %s runtime (dry-run)", runtimeName)
	logger.Info(message)

	return message, nil
}

func (runtime OpenfaasRuntime) Build(ctx context.Context) error {
	repository := NewRepository(classicRuntimeRepo)

	// FunctionRuntime will look like openfaas/classic/ruby
	_, runtimeName := path.Split(runtime.FunctionRuntime)

	if len(runtimeName) == 0 {
		message := fmt.Sprintf("runtime name \"%s\" is incomplete", runtime.FunctionRuntime)
		err := errors.New(message)
		logger.WithError(err).Warn(message)

		return err
	}

	logger.Infof("Using OpenFaaS %s runtime", runtimeName)

	err := runtime.writeRuntimeFiles(repository, runtimeName)

	return err
}

func (runtime OpenfaasRuntime) DockerfilePath() string {
	return path.Join(runtime.CodeDirectory, "Dockerfile")
}

func (runtime OpenfaasRuntime) writeRuntimeFiles(repository *Repository, runtimeName string) error {
	// OpenFaaS runtimes are conventionally located within a directory with the pattern template/{{runtime_name}}/
	runtimeDirectory := path.Join(templateDirectory, runtimeName)

	files, err := repository.ListFiles(runtimeDirectory)
	if err != nil {
		return err
	}

	for _, file := range files {
		// The function directory contains a sample handler function, which we want to ignore
		if file.Name() == ignoreDirectory {
			continue
		}

		fileContents, err := repository.ReadFile(path.Join(runtimeDirectory, file.Name()))
		if err != nil {
			return err
		}

		err = fs.WriteFile(path.Join(runtime.CodeDirectory, file.Name()), []byte(fileContents), 0644)
		if err != nil {
			return err
		}
	}

	return nil
}
