package runtime

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDockerfilePath(t *testing.T) {
	runtime := DockerfileRuntime{Details: Details{FunctionName: "my func"}}
	assert.Equal(t, "Dockerfile", runtime.DockerfilePath())

	runtime = DockerfileRuntime{Details: Details{CodeDirectory: "echo"}}
	assert.Equal(t, "echo/Dockerfile", runtime.DockerfilePath())
}

func TestDockerfileRuntimeBuild(t *testing.T) {
	runtime := DockerfileRuntime{Details{
		FunctionName:  "my func",
		CodeDirectory: "my/function/",
	}}

	err := runtime.Build(context.Background())

	assert.NoError(t, err)
}

func TestDockerfileRuntimeBuildDryRun(t *testing.T) {
	runtime := DockerfileRuntime{Details{
		FunctionName:  "my func",
		CodeDirectory: "my/function/",
	}}

	summary, err := runtime.BuildDryRun()

	assert.Equal(t, "using a Dockerfile runtime", summary)
	assert.NoError(t, err)
}
