package runtime

import (
	"context"
	"path"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

type DockerfileRuntime struct {
	Details
}

// Build runtime, in case of a Dockerfile runtime just log the action
func (runtime DockerfileRuntime) Build(ctx context.Context) error {
	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("using a Dockerfile runtime")

	return nil
}

// Build runtime DryRun
func (runtime DockerfileRuntime) BuildDryRun() (string, error) {
	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("using a Dockerfile runtime (DryRun)")

	return "using a Dockerfile runtime", nil
}

// Return an expected path to a Dockerfile
func (runtime DockerfileRuntime) DockerfilePath() string {
	return path.Join(runtime.CodeDirectory, "Dockerfile")
}
