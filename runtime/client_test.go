package runtime

import (
	"os"
	"testing"
	"time"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func TestClient(t *testing.T) {
	createRuntimeRepository(t)
	defer removeRuntimeRepository(t)

	t.Run("TestNewClient", func(t *testing.T) {
		_, err := newTestClient()

		assert.NoError(t, err)
	})

	t.Run("TestClientReadFile", func(t *testing.T) {
		client, err := newTestClient()
		require.NoError(t, err)

		function, err := client.ReadFile("function.rb")
		require.NoError(t, err)

		template, err := client.ReadFile("Dockerfile.template")
		require.NoError(t, err)

		assert.Contains(t, function, "self.invoke")
		assert.Contains(t, template, "FROM my-custom-invoker")
	})

	t.Run("TestClientWriteFiles", func(t *testing.T) {
		client, err := newTestClient()
		require.NoError(t, err)

		fs.WithTestFs(func() {
			err = client.WriteFiles("sub/directory")
			require.NoError(t, err)

			invoker, err := fs.ReadFile("sub/directory/function.rb")
			require.NoError(t, err)

			template, err := fs.ReadFile("sub/directory/Dockerfile.template")
			require.NoError(t, err)

			assert.Contains(t, string(invoker), "self.invoke")
			assert.Contains(t, string(template), "FROM my-custom-invoker")
		})
	})

	t.Run("ListFiles", func(t *testing.T) {
		client, err := newTestClient()
		require.NoError(t, err)

		files, err := client.ListFiles(".")

		filenames := make([]string, len(files))
		for i, file := range files {
			filenames[i] = file.Name()
		}

		assert.Len(t, files, 3)
		assert.Subset(t, filenames, []string{"Dockerfile.template", "function.rb", "README.md"})
	})
}

func newTestClient() (Client, error) {
	return NewClient(Location{
		Address:   "testdata/runtime",
		Reference: "master",
	})
}

func removeRuntimeRepository(t *testing.T) {
	err := os.RemoveAll("testdata/runtime/.git")
	require.NoError(t, err)
}

func createRuntimeRepository(t *testing.T) {
	repository, err := git.PlainInit("testdata/runtime", false)
	require.NoError(t, err)

	worktree, err := repository.Worktree()
	require.NoError(t, err)

	_, err = worktree.Add(".")
	require.NoError(t, err)

	_, err = worktree.Commit("Initial commit", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "John Doe",
			Email: "john@doe.org",
			When:  time.Now(),
		},
	})
	require.NoError(t, err)
}
