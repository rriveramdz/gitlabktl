package knative

import (
	"github.com/stretchr/testify/mock"
)

type MockConfig struct {
	mock.Mock
}

func (config *MockConfig) Namespace() string {
	args := config.Called()

	return args.String(0)
}

func (config *MockConfig) KubeConfig() string {
	args := config.Called()

	return args.String(0)
}

func (config *MockConfig) Registry() Registry {
	args := config.Called()

	return args.Get(0).(Registry)
}
