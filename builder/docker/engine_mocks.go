package docker

import (
	"context"
	"io"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
)

type MockEngine struct {
	mock.Mock
}

func (engine MockEngine) Ping(ctx context.Context) (types.Ping, error) {
	args := engine.Called()

	return types.Ping{}, args.Error(1)
}

func (engine MockEngine) ImageBuild(ctx context.Context, reader io.Reader, opts types.ImageBuildOptions) (types.ImageBuildResponse, error) {
	args := engine.Called()

	return types.ImageBuildResponse{}, args.Error(1)
}
