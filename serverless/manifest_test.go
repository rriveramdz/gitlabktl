package serverless

import (
	"io/ioutil"
	"testing"

	"github.com/MakeNowJust/heredoc"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func newServerlessConfig(t *testing.T, yaml ...string) (manifest Manifest, err error) {
	var fixture []byte

	if len(yaml) == 0 {
		fixture, err = ioutil.ReadFile("testdata/serverless.yml")
		require.NoError(t, err)
	} else {
		fixture = []byte(yaml[0])
	}

	fs.WithTestFs(func() {
		err = fs.WriteFile(ServerlessFile, fixture, 0644)
		require.NoError(t, err)

		manifest, err = NewServerlessConfig()
	})

	return manifest, err
}

func TestNewServerlessConfig(t *testing.T) {
	t.Run("when configuration is valid", func(t *testing.T) {
		config, err := newServerlessConfig(t)
		require.NoError(t, err)

		assert.Equal(t, "my-functions", config.Service)
	})

	t.Run("when config does not exist", func(t *testing.T) {
		fs.WithTestFs(func() {
			config, err := NewServerlessConfig()

			assert.Equal(t, Manifest{}, config)
			assert.Equal(t, "could not find serverless manifest file", err.Error())
		})
	})

	t.Run("when config using triggermesh provider is valid", func(t *testing.T) {
		fs.WithTestFs(func() {
			yaml := heredoc.Doc(`
				service: test
				provider:
				  name: triggermesh

				functions:
				  echo:
				    handler: Echo.run
			`)

			config, err := newServerlessConfig(t, yaml)

			assert.Equal(t, "test", config.Service)
			assert.NoError(t, err)
		})
	})

	t.Run("when config using gitlab/knative provider is valid", func(t *testing.T) {
		fs.WithTestFs(func() {
			yaml := heredoc.Doc(`
				service: test
				provider:
				  name: gitlab/knative

				functions:
				  echo:
				    handler: Echo.run
			`)

			config, err := newServerlessConfig(t, yaml)

			assert.Equal(t, "test", config.Service)
			assert.NoError(t, err)
		})
	})

	t.Run("when config contains unknown provider", func(t *testing.T) {
		fs.WithTestFs(func() {
			yaml := heredoc.Doc(`
				service: test
				provider:
				  name: invalid

				functions:
				  echo:
				    handler: Echo.run
			`)

			config, err := newServerlessConfig(t, yaml)

			assert.Equal(t, "test", config.Service)
			assert.EqualError(t, err, "invalid provider: `invalid` provider is not supported")
		})
	})

	t.Run("when config does not have service definition", func(t *testing.T) {
		fs.WithTestFs(func() {
			yaml := heredoc.Doc(`
				provider:
				  name: gitlab/knative

				functions:
				  echo:
				    handler: Echo.run
			`)

			_, err := newServerlessConfig(t, yaml)

			assert.EqualError(t, err, "service name can't be empty")
		})
	})

	t.Run("when config does not have functions", func(t *testing.T) {
		fs.WithTestFs(func() {
			yaml := heredoc.Doc(`
				service: test
				provider:
				  name: gitlab/knative
			`)

			_, err := newServerlessConfig(t, yaml)

			assert.EqualError(t, err, "config does not have functions defined")
		})
	})
}

func TestServerlessFunctions(t *testing.T) {
	t.Run("when testing serverless fixture", func(t *testing.T) {
		manifest, err := newServerlessConfig(t)
		require.NoError(t, err)

		functions := manifest.ToFunctions()
		fn := functions[0]

		assert.Equal(t, 1, len(functions))
		assert.Equal(t, "echo", fn.Name)
		assert.Equal(t, "my-functions-echo", fn.Service)
		assert.Contains(t, fn.Runtime, "nodejs")
		assert.Contains(t, fn.Directory, "gitlabktl/serverless")
		assert.Contains(t, fn.Description, "echo function")
		assert.Contains(t, fn.Envs, "MY_ENV")
		assert.Contains(t, fn.Envs, "LEGACY_ENV")
		assert.Contains(t, fn.Envs, "MY_PROVIDER_ENV")
		assert.Contains(t, fn.Envs, "MY_LEGACY_PROVIDER_ENV")
		assert.Contains(t, fn.Secrets, "my-k8s-secret")
		assert.Contains(t, fn.Secrets, "my-new-k8s-secret")
		assert.Contains(t, fn.Secrets, "my-new-provider-k8s-secret")
		assert.Contains(t, fn.Secrets, "my-legacy-provider-k8s-secret")
		assert.Contains(t, fn.Annotations, "my-annotation")
		assert.Contains(t, fn.Annotations, "provider-annotation")
		assert.Equal(t, "registry.gitlab.com/my-project/my-functions-echo", fn.Image)
	})

	t.Run("when a manifest does not have some fields defined", func(t *testing.T) {

		fs.WithTestFs(func() {
			yaml := heredoc.Doc(`
				service: my-functions
				description: my serverless functions
				provider:
				  name: triggermesh

				functions:
				  echo:
				    handler: Echo.run
			`)

			env.WithStubbedEnv(env.Stubs{"CI_REGISTRY_IMAGE": "my.registry/my-project"}, func() {
				manifest, err := newServerlessConfig(t, yaml)
				require.NoError(t, err)

				fn := manifest.ToFunctions()[0]

				assert.Equal(t, "my serverless functions", fn.Description)
				assert.Equal(t, "my.registry/my-project/my-functions-echo", fn.Image)
			})
		})
	})
}
